*** Settings ***
Library  Selenium2Library

*** Variables ***
${navbar_menu_containter}   //img[@alt="logo-flip"]/ancestor::a/following-sibling::div
${button_produk}            ${navbar_menu_containter}/div/button
${button_karir}             ${navbar_menu_containter}/a[@href="http://flip.id/karir"]/button
${button_masuk}             ${navbar_menu_containter}/button

*** Keywords ***
Opening Browser
    [Arguments]   ${site_homepage}  ${browser}
    Open Browser  ${site_homepage}  ${browser}
    Maximize Browser Window
    
Click Button Produk
    [Timeout]    5
    Wait Until Element Is Visible  ${button_produk}
    Element Text Should Be         ${button_produk}     Produk      message=Invalid text on button Produk
    Click Button                   ${button_produk}

Click Button Karir
    [Timeout]    5
    Wait Until Element Is Visible  ${button_karir}      
    Element Text Should Be         ${button_karir}      Karir      message=Invalid text on button Karir
    Click Button                   ${button_karir}

Click Button Masuk
    [Timeout]    5
    Wait Until Element Is Visible  ${button_masuk}      
    Element Text Should Be         ${button_masuk}      Masuk      message=Invalid text on button Masuk
    Click Button                   ${button_masuk}
