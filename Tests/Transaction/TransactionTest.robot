*** Settings ***
Documentation  Transaction scenario, validate data between UI and DB record
Library   Selenium2Library
Library   DatabaseLibrary
Variables   ../../Resources/Transaction/properties.py
Suite Setup         Access Homepage
Suite Teardown      Close Browser

*** Test Case ***
TransactionTest - Validate Transaction Data
    [Documentation]  Verify data displayed on web referring to db
    ${transactions}  Get WebElements  //tbody/tr
    FOR  ${transaction-row}  IN  @{transactions}
         ${transaction}  Get Child WebElements  ${transaction-row}  td
         Validate Displayed Transaction         ${transaction}
    END

*** Keywords ***
Access Homepage
    [Documentation]  Open web browser, go to homepage
    Open Browser         ${site_homepage}  chrome

Validate Displayed Transaction
    [Documentation]      Validate displayed data in UI with database record
    [Arguments]          ${actual_trx}
    ${trx_id}            Get Text  ${actual_trx[0]}
    ${query_string}      Catenate  Select transaction_id, username, source_bank,destination_bank, amount
    ...                            From transactions where transaction_id = "${trx_id}"
    Connect To Database  ${database_api}  ${database_name}  ${database_user}  ${database_password}  ${database_host}  ${database_port}
    ${expected_trx}      Query    ${query_string}
    Disconnect From Database
    Length Should Be     ${expected_trx}   1  message=Transaction with ID ${trx_id} not found in database
    Element Text Should Be  ${actual_trx[1]}  ${expected_trx[0][1]}  message=User name mismatched
    Element Text Should Be  ${actual_trx[2]}  ${expected_trx[0][2]}  message=Bank source mismatched
    Element Text Should Be  ${actual_trx[3]}  ${expected_trx[0][3]}  message=Destination bank mismatched
    Element Text Should Be  ${actual_trx[4]}  ${expected_trx[0][4]}  message=Transfer amount mismatched

Get Child WebElements
    [Documentation]     Get child of web elements by xpath
    [Arguments]    ${parent}   ${child}  
    ${children}    Call Method   ${parent}  find_elements  by=xpath  value=child::${child}    
    [Return]       ${children}
    