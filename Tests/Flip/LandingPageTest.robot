*** Settings ***
Documentation  UI scenarios to verify landing page navigation menu
Library   Selenium2Library
Variables   ../../Resources/Flip/properties.py
Resource    ../../Resources/Flip/PageObjects/Common.robot
Suite Setup         Opening Browser  ${site_homepage}  ${browser}
Test Setup          Go To   ${site_homepage}
Suite Teardown      Close Browser

*** Test Case ***
Verify Button Produk
    Click Button Karir

Verify Button Karir
    Click Button Produk

Verify Button Masuk
    Click Button Masuk
