*** Settings ***
Library  REST  https://reqres.in/
Library  String

*** Variables ***
${endpoint}        /api/users

*** Test Cases ***
Verify GET list of users
    [Documentation]  Get list of users in first page  
    GET             ${endpoint}
    Integer         response status           200
    ${response}     Object                    response body
    Integer         response body page                  1
    Integer         response body per_page              6
    Integer         response body total                 minimum=1
    Integer         response body total_pages           minimum=1   
    ${users}        Array    response body data         minItems=1  uniqueItems=true
    Verify List of Users   @{users[0]}     
    [Teardown]  Output schema 

Verify POST create a user
    [Documentation]   Create a new users
    ${name} 	    Generate Random String	 10	[LOWER]
    ${job}          Generate Random String	 10	[LOWER]
    ${request}      Set Variable             {"name": "${name}", "job": "${job}"}
    POST            ${endpoint}              ${request}
    Integer         response status          201
    ${response}     Object                   response body
    String          response body name       ${name}
    String          response body job        ${job}
    String          response body id
    String          response body createdAt
    [Teardown]  Output schema   


*** Keywords ***
Verify List of Users
    [Documentation]     Validate user data structure
    [Arguments]         @{users}
    FOR  ${user}   IN   @{users}
         Should Be true             ${user['id']}!=${NULL}
         Should Not Be Empty        ${user['email']}
         Should Not Be Empty        ${user['first_name']}
         Should Not Be Empty        ${user['last_name']}
         Should Not Be Empty        ${user['avatar']}
    END
