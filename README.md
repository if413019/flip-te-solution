# Flip TE Technical Test - Solution
Test Engineer Technical Test Solution @Flip, implemented using robot framework.

## Pre-requisites
- Python 3 https://www.python.org/downloads/
- Pip https://pypi.org/project/pip/
- chrome browser & chrome driver https://chromedriver.chromium.org/downloads

## Installation
This installation instruction is made for windows machine, please adjust if you're using different OS
- (Optional, but recommended) Install, create, and activate python virtual env.
  - `sudo pip3 install virtualenv`
  - `virtualenv venv`
  - `venv\Scripts\activate.bat`
- Install all requirements `pip install -r requirements.txt`
- Execute test files `robot Tests/Transaction/TransactionTest.robot` or `robot Tests/Flip/LandingPageTest.robot` or `robot Tests/Reqres/Users.robot`
- Observe test result on generated files

## Solo Test Engineer Priority
- As a solo test engineer in a team, the highest priority job desc should be to make sure features that being developed are comprehensively tested & met the quality criteria. The goals is to minimize defects released to production environment that will impact the development cycle which means more time to testing.
Less defects released to production = Less back and forth testing = more time to automate, documentation, and other improvement that will impact to the team growth
